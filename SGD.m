function weight = SGD (weight,input,correct_output)
    alpha = 0.5;
    N = 4;
    for k=1:N
        t_input = input(k,:)';
        d = correct_output(k);
        weighted_sum = weight * t_input;
        output = sigmoid (weighted_sum);
        error = d - output;
        delta = output * (1-output) * error;
        dweight = alpha * delta * t_input;
        
        weight(1)= weight(1) + dweight(1);
        weight(2)= weight(2) + dweight(2);
        weight(3)= weight(3) + dweight(3);
    end
end
                